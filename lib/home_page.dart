import 'package:flutter/material.dart';
import 'package:tugas/profil_page.dart';

class HomePage extends StatelessWidget {
  final namaey;
  var poto = 'assets/ahmad.jpeg';
  var poto1 = 'assets/bugi.png';
  var poto2 = 'assets/azka.jpeg';
  var poto3 = 'assets/asep.jpeg';
  var poto4 = 'assets/hafid.jpeg';
  var nama1 ='Ahmad Hermansyah';
  var nama2 ='Reza Dwi Putra';
  var nama3 = 'Azka Ryvaldo Nurramadan';
  var nama4 = 'Asep Saepul Ulum Bahri';
  var nama5 = 'Hafidt Amanulloh Multyazi';
  var npm = '18111181';
  var npm1 = '18111389';
  var npm2 = '18111187';
  var npm3 = '13111055';
  var npm4 = '18111388';
  var alamat = 'Kp.Saradan RT 07 RW 02 NO.272 Kel.Leuwigajah Kec.Cimahi Selatan';
  var alamat1 = 'Kp.Cilebak RT 05 RW 02 Desa Rancamanyar Kec.Baleendah Kab.Bandung';
  var alamat2 = 'jl.Cibaduyut gg mamaja No.18';
  var alamat3 = 'Jl.Kopo gg Ekawarga II No.43';
  var alamat4 = 'Jl.Leuwipanjang N0.64';
  var hobi = 'Olahraga dan Berbagi Kebahagiaan';
  var hobi1 = 'Playing Game Online';
  var hobi2 = 'Rebahan';
  var hobi3 = 'Game dan Backpacker';
  var hobi4 = 'Ngevape';

  HomePage({@required this.namaey});
  static String tag = 'home-page';

  @override
  Widget build(BuildContext context) {
    final ahmad = Hero(
      tag: 'hero',
      child: Padding(
        padding: EdgeInsets.all(16.0),
        child: CircleAvatar(
          radius: 72.0,
          backgroundColor: Colors.transparent,
          backgroundImage: AssetImage('assets/ahmad.jpeg'),
        ),
      ),
    );

     final asep = Hero(
      tag: 'herooo',
      child: Padding(
        padding: EdgeInsets.all(16.0),
        child: CircleAvatar(
          radius: 72.0,
          backgroundColor: Colors.transparent,
          backgroundImage: AssetImage('assets/asep.jpeg'),
        ),
      ),
    );

    final azka = Hero(
      tag: 'heroooo',
      child: Padding(
        padding: EdgeInsets.all(16.0),
        child: CircleAvatar(
          radius: 72.0,
          backgroundColor: Colors.transparent,
          backgroundImage: AssetImage('assets/azka.jpeg'),
        ),
      ),
    );

    final hafid = Hero(
      tag: 'herooooo',
      child: Padding(
        padding: EdgeInsets.all(16.0),
        child: CircleAvatar(
          radius: 72.0,
          backgroundColor: Colors.transparent,
          backgroundImage: AssetImage('assets/hafid.jpeg'),
        ),
      ),
    );

    final reza = Hero(
      tag: 'heroo',
      child: Padding(
        padding: EdgeInsets.all(16.0),
        child: CircleAvatar(
          radius: 72.0,
          backgroundColor: Colors.transparent,
          backgroundImage: AssetImage('assets/bugi.png'),
        ),
      ),
    );

    final welcome = Padding(
      padding: EdgeInsets.all(8.0),
      child: Text(
        'Selamat Datang',
        style: TextStyle(fontSize: 28.0, color: Colors.white),
      ),
    );

    final loginprofilahmad = Padding(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      child: Material(
        borderRadius: BorderRadius.circular(30.0),
        shadowColor: Colors.lightBlueAccent.shade100,
        elevation: 5.0,
        child: MaterialButton(
          minWidth: 200.0,
          height: 42.0,
          onPressed: () {
            Navigator.push(context, MaterialPageRoute(
              builder: (_) {
              var profilPage = ProfilPage(
                poto: poto,
                nama1: nama1,
                npm: npm,
                alamat: alamat,
                hobi: hobi,
              );
            return profilPage;
            },
            ));
          },
          color: Colors.lightBlueAccent,
          child: Text('Buka Profil', style: TextStyle(color: Colors.white)),
        ),
      ),
    );

final loginprofilasep = Padding(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      child: Material(
        borderRadius: BorderRadius.circular(30.0),
        shadowColor: Colors.lightBlueAccent.shade100,
        elevation: 5.0,
        child: MaterialButton(
          minWidth: 200.0,
          height: 42.0,
          onPressed: () {
            Navigator.push(context, MaterialPageRoute(
              builder: (_) {
              var profilPage = ProfilPage(
                poto: poto3,
                nama1: nama4,
                npm: npm3,
                alamat: alamat3,
                hobi: hobi3,
              );
            return profilPage;
            },
            ));
          },
          color: Colors.lightBlueAccent,
          child: Text('Buka Profil', style: TextStyle(color: Colors.white)),
        ),
      ),
    );

final loginprofilazka = Padding(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      child: Material(
        borderRadius: BorderRadius.circular(30.0),
        shadowColor: Colors.lightBlueAccent.shade100,
        elevation: 5.0,
        child: MaterialButton(
          minWidth: 200.0,
          height: 42.0,
          onPressed: () {
            Navigator.push(context, MaterialPageRoute(
              builder: (_) {
              var profilPage = ProfilPage(
                poto: poto2,
                nama1: nama3,
                npm: npm2,
                alamat: alamat2,
                hobi: hobi2,
              );
            return profilPage;
            },
            ));
          },
          color: Colors.lightBlueAccent,
          child: Text('Buka Profil', style: TextStyle(color: Colors.white)),
        ),
      ),
    );

final loginprofilhafid = Padding(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      child: Material(
        borderRadius: BorderRadius.circular(30.0),
        shadowColor: Colors.lightBlueAccent.shade100,
        elevation: 5.0,
        child: MaterialButton(
          minWidth: 200.0,
          height: 42.0,
          onPressed: () {
            Navigator.push(context, MaterialPageRoute(
              builder: (_) {
              var profilPage = ProfilPage(
                poto: poto4,
                nama1: nama5,
                npm: npm4,
                alamat: alamat4,
                hobi: hobi4,
              );
            return profilPage;
            },
            ));
          },
          color: Colors.lightBlueAccent,
          child: Text('Buka Profil', style: TextStyle(color: Colors.white)),
        ),
      ),
    );

     final loginprofilbugi = Padding(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      child: Material(
        borderRadius: BorderRadius.circular(30.0),
        shadowColor: Colors.lightBlueAccent.shade100,
        elevation: 5.0,
        child: MaterialButton(
          minWidth: 200.0,
          height: 42.0,
          onPressed: () {
            Navigator.push(context, MaterialPageRoute(
              builder: (_) {
              var profilPage = ProfilPage(
                poto: poto1,
                nama1: nama2,
                npm: npm2,
                alamat: alamat1,
                hobi: hobi1,
              );
            
            return profilPage;
            },
            ));
          },
          color: Colors.lightBlueAccent,
          child: Text('Buka Profil', style: TextStyle(color: Colors.white)),
        ),
      ),
    );

    final body = Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.all(28.0),
      decoration: BoxDecoration(
        gradient: LinearGradient(colors: [
          Colors.blue,
          Colors.lightBlueAccent,
        ]),
      ),
      child: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        padding: EdgeInsets.all(28.0),
        child:  Column(
        children: <Widget>[welcome, ahmad, loginprofilahmad, asep, loginprofilasep, azka, loginprofilazka, hafid, loginprofilhafid, reza, loginprofilbugi]
      ),
      ) 
     
    );

    return Scaffold(
      body: body,
    );
  }
}