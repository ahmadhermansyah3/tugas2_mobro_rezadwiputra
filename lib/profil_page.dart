import 'package:flutter/material.dart';

class ProfilPage extends StatelessWidget {
  final poto,nama1,npm,alamat,hobi;
  ProfilPage({@required this.poto,@required this.nama1,@required this.npm,@required this.alamat,@required this.hobi});
  static String tag = 'profil-page';

  @override
  Widget build(BuildContext context) {
    final ahmad = Hero(
      tag: 'hero',
      child: Padding(
        padding: EdgeInsets.all(16.0),
        child: CircleAvatar(
          radius: 72.0,
          backgroundColor: Colors.transparent,
          backgroundImage: AssetImage('$poto'),
        ),
      ),
    );

    final welcome = Padding(
      padding: EdgeInsets.all(8.0),
      child: Text(
        'Welcome',
        style: TextStyle(fontSize: 28.0, color: Colors.black),
      ),
    );

    final ahmad1 = Padding(
      padding: EdgeInsets.all(8.0),
      child: Text(
        'Nama : $nama1'
        '\nNPM : $npm'
        '\nAlamat : $alamat'
        '\nHobi : $hobi',
        style: TextStyle(fontSize: 16.0, color: Colors.black),
      ),
    );
    

    final body = Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.all(28.0),
      decoration: BoxDecoration(
        gradient: LinearGradient(colors: [
          Colors.green,
          Colors.lightGreenAccent,
        ]),
      ),
      child: Column(
        children: <Widget>[welcome, ahmad, ahmad1]
      ),
    );

    return Scaffold(
      body: body,
    );
  }
}