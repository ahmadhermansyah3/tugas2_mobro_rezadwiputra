import 'package:flutter/material.dart';
import 'home_page.dart';

class LoginPage extends StatefulWidget {
  static String tag = 'login-page';
  @override
  _LoginPageState createState() => new _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController textnama = TextEditingController();
  @override
  Widget build(BuildContext context) {
    final logo = Hero(
      tag: 'hero',
      child: CircleAvatar(
        backgroundColor: Colors.transparent,
        radius: 48.0,
        child: Image.asset('assets/sttb.png'),
      ),
    );

    final nama = TextFormField(
      keyboardType: TextInputType.name,
      controller: textnama,
      autofocus: false,
      style: TextStyle(
        color: Colors.white 
      ),
      decoration: InputDecoration(
        hintText: 'Masukan Nama Anda',
        fillColor: Colors.white,
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );

    final loginButton = Padding(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      child: Material(
        borderRadius: BorderRadius.circular(30.0),
        shadowColor: Colors.lightBlueAccent.shade100,
        elevation: 5.0,
        child: MaterialButton(
          minWidth: 200.0,
          height: 42.0,
          onPressed: () {
            var namaey = textnama.text;
            Navigator.push(context, MaterialPageRoute(
              builder: (_) {
                var homePage = HomePage(
                  namaey: namaey.toString(),
                );

                return homePage;
              },
            ));
          },
          color: Colors.lightBlueAccent,
          child: Text('Log In', style: TextStyle(color: Colors.white)),
        ),
      ),
    );

    return Scaffold(
       body: Center(
        child: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/8.jpg'),
              fit: BoxFit.cover
            ),
            // gradient: LinearGradient(colors: []),
          ),
          child: ListView(
            padding: EdgeInsets.only(top: 180.0, left: 24.0, right: 24.0),
            children: <Widget>[
              logo,
              SizedBox(height: 48.0),
              nama,
              SizedBox(height: 8.0),
              loginButton,
            ],
          ),
        ),
       ),
     );  
  }
}
